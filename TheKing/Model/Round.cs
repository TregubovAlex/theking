﻿using System;

namespace TheKing
{
    internal sealed class RoundKind
    {
        private readonly int enumId;
        private readonly String name;
        private readonly String fullName;
        private readonly int numOfCards;
        private readonly int priceForCard;
        internal static readonly RoundKind NV = new RoundKind(1, "НВ", "Не брать взятки", 10, -4);
        internal static readonly RoundKind Ch = new RoundKind(2, "Ч", "Не брать черви", 8, -5);
        internal static readonly RoundKind Ma = new RoundKind(3, "М", "Не брать мальчиков", 8, -5);
        internal static readonly RoundKind De = new RoundKind(4, "Д", "Не брать дам", 4, -10);
        internal static readonly RoundKind PV = new RoundKind(5, "ПВ", "Не брать последние взятки", 2, -20);
        internal static readonly RoundKind Ki = new RoundKind(6, "К", "Не брать Кинга", 1, -40);
        internal static readonly RoundKind k1 = new RoundKind(7, "1", "Козырь", 10, 6);
        internal static readonly RoundKind k2 = new RoundKind(8, "2", "Козырь", 10, 6);
        internal static readonly RoundKind k3 = new RoundKind(9, "3", "Козырь", 10, 6);
        internal static readonly RoundKind k4 = new RoundKind(10, "4", "Козырь", 10, 6);
        private RoundKind(int _enumId, String _name, String _fullname, int _numOfCards, int _priceForCard)
        {
            enumId = _enumId;
            name = _name;
            fullName = _fullname;
            numOfCards = _numOfCards;
            priceForCard = _priceForCard;
        }
        internal int EnumId
        { get { return enumId; } }
        internal string Name
        { get { return name; } }
        internal string FullName
        { get { return fullName; } }
        internal int NumOfCards
        { get { return numOfCards; } }
        internal int PriceForCard
        { get { return priceForCard; } }
    }
    internal class Round
    {
        int num;
        RoundKind kind;
        int[] results;
        internal int Num
        {
            get { return num; }
            set { num = value; }
        }
        internal RoundKind Kind
        {
            get { return kind; }
            set { kind = value; }
        }
        internal int[] Results
        {
            get { return results; }
            set { results = value; }
        }
        internal Round(string _kindName)
        {
            switch (_kindName)
            {
                case "НВ": kind = RoundKind.NV; break;
                case "Ч": kind = RoundKind.Ch; break;
                case "М": kind = RoundKind.Ma; break;
                case "Д": kind = RoundKind.De; break;
                case "ПВ": kind = RoundKind.PV; break;
                case "К": kind = RoundKind.Ki; break;
                case "1": kind = RoundKind.k1; break;
                case "2": kind = RoundKind.k2; break;
                case "3": kind = RoundKind.k3; break;
                case "4": kind = RoundKind.k4; break;
            }
        }
    }
}