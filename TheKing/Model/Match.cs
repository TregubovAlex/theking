﻿using System;
using System.Collections.Generic;

namespace TheKing
{
    internal class Match
    {
        int num;
        DateTime created;
        List<Player> players = new List<Player>();
        int gamingPlayerPosition;
        string selectedRoundKindName;
        internal int Num
        {
            get { return num; }
            set { num = value; }
        }
        internal DateTime Created
        {
            get { return created; }
            set { created = value; }
        }
        internal List<Player> Players
        {
            get { return players; }
            set { players = value; }
        }
        internal int GamingPlayerPosition
        {
            get { return gamingPlayerPosition; }
            set { gamingPlayerPosition = value; }
        }
        internal string SelectedRoundKindName
        {
            get { return selectedRoundKindName; }
            set { selectedRoundKindName = value; }
        }
        internal Match()
        {
            num = 0;
            created = DateTime.Now;
            gamingPlayerPosition = 0;
        }
        internal void AddPlayer(int _num, string _name)
        {
            Player player = new Player(_num, _name, players.Count);
            players.Add(player);
        }
        internal void NextGamingPlayerPosition()
        {
            gamingPlayerPosition++;
            if (gamingPlayerPosition == players.Count)
                gamingPlayerPosition = 0;
        }
    }
}