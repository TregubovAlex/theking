﻿namespace TheKing
{
    internal class Player
    {
        int num;
        string name;
        int position;
        internal int Num
        {
            get { return num; }
            set { num = value; }
        }
        internal string Name
        {
            get { return name; }
            set { name = value; }
        }
        internal int Position
        {
            get { return position; }
            set { position = value; }
        }
        internal Player(int _num, string _name, int _position)
        {
            num = _num;
            name = _name;
            position = _position;
        }
    }
}