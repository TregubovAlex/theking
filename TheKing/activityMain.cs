﻿using System;
using System.IO;
using System.Xml.Linq;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;

namespace TheKing
{
    [Activity(MainLauncher = true, Icon = "@drawable/icon")]//Label = "TheKing",
    internal class activityMain : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.layoutMain);

            DBConn.Instance();

            /*try
            {
                Stream tempCheckFileStream = OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename));
                tempCheckFileStream.Close();
            }
            catch (Java.IO.FileNotFoundException ex) //базы нет, первый запуск
            {
                CreateBlankDataFile();
            }*/



            //Чтение с SD_card
            /*String sdState = Android.OS.Environment.ExternalStorageState; //Получаем состояние SD карты (подключена она или нет) - возвращается true и false соответственно
            string folder;
            if (sdState.Equals(Android.OS.Environment.MediaMounted)) // если true
            {
                folder = Android.OS.Environment.ExternalStorageDirectory.ToString();
                XDocument docSD = XDocument.Load(folder + "/TheKing_data_copy.xml");
                //doc.Save(folder + "/TheKing_data_copy.xml");
            }*/

            //Запись файла целиком во внутреннюю память
            /*Stream inputStream = new MemoryStream();
            doc.Save(inputStream);
            inputStream.Position = 0;
            Stream outputStream = OpenFileOutput(filename, FileCreationMode.Private);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.Read(buf, 0, buf.Length)) > 0)
                outputStream.Write(buf, 0, len);
            inputStream.Close();
            outputStream.Close();*/


            //Чтение из внутренней прамяти
            /*Stream input = OpenFileInput(filename);
            XDocument docNew = XDocument.Load(input);
            foreach (XElement elem in docNew.Root.Elements("player"))
            {
                Toast.MakeText(this, elem.Name + "=" + elem.Attribute("name").Value, ToastLength.Short).Show();
                //foreach (XAttribute attr in elem.Attributes())
                //    Toast.MakeText(this, attr.Value.ToString(), ToastLength.Short).Show();
            }*/



            Button buttonCreate = FindViewById<Button>(Resource.Id.buttonCreate);
            buttonCreate.Click +=
                delegate
                #region
                {
                    Intent intent = new Intent(this, typeof(activityPlayers));
                    StartActivity(intent);
                };
                #endregion
            Button buttonOpen = FindViewById<Button>(Resource.Id.buttonOpen);
            buttonOpen.Click +=
                delegate
                #region
                {
                    Intent intent = new Intent(this, typeof(activityMatches));
                    StartActivity(intent);
                };
                #endregion

            
            //buttonCreate.Enabled = false;

            Button buttonResetDatabase = FindViewById<Button>(Resource.Id.buttonResetDatabase);
            buttonResetDatabase.Click +=
                delegate
                #region
                {
                    DBConn.Instance().CreateBlankDataFile();
                };
                #endregion
            /*Button buttonCopyDatabaseToSD = FindViewById<Button>(Resource.Id.buttonCopyDatabaseToSD);
            buttonCopyDatabaseToSD.Click +=
                delegate
                #region
                {
                    dc.BackUpDatabaseToSDCard(OpenFileInput(dc.DataFileName));
                };
                #endregion*/
        }
        /*private void ReadFromDatafile()
        {
            //Чтение из внутренней прамяти
            try
            {
                Stream input = OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename));
                XDocument docNew = XDocument.Load(input);
                //XDocument docNew = XDocument.Load(filename);
                foreach (XElement elem in docNew.Root.Elements())
                {
                    //Toast.MakeText(this, elem.Name + " = " + elem.Attribute("name").Value, ToastLength.Short).Show();
                    //foreach (XAttribute attr in elem.Attributes())
                    //    Toast.MakeText(this, attr.Value.ToString(), ToastLength.Short).Show();
                }
            }
            catch (Java.IO.FileNotFoundException ex)
            {
                Toast.MakeText(this, "Чтение: " + ex.Message, ToastLength.Short).Show();
            }
        }*/
        
    }
}