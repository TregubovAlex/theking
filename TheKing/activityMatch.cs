﻿using System;
using System.Collections.Generic;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Linq;
using System.Xml.Linq;
using System.IO;

namespace TheKing
{
    [Activity] //(Label = "activityMatch")
    internal class activityMatch : Activity
    {
        DBConn dc = DBConn.Instance(); //new DataController();
        Match match;
        int[] checkedPlayersNums;
        List<TextView>[] arrayListsOfPlayersRounds;
        View[] arrayViewsPlayers;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.layoutMatch);
            LinearLayout linearLayoutMatch = FindViewById<LinearLayout>(Resource.Id.linearLayoutMatch);

            match = new Match();
            //XDocument doc;
            XElement matchXML;
            bool isNewMatch = Intent.Extras.GetBoolean("isNewMatch");
            if (isNewMatch)
            {
                checkedPlayersNums = Intent.Extras.GetIntArray("checkedPlayersNums");
                AddMatchToData();
            }
            else //открываем ранее начатый матч
            {
                match.Num = Intent.Extras.GetInt("matchNum");
                //doc = dc.ReadDocFromSDCard();
                matchXML = DBConn.Instance().Doc.Root.Elements("match").Where(r => r.Attribute("num").Value == match.Num.ToString()).First();
                match.Created = Convert.ToDateTime(matchXML.Attribute("date").Value);

                string[] checkedPlayersNumsSTRING = matchXML.Attribute("players").Value.ToString().Split('_');
                checkedPlayersNums = new int[checkedPlayersNumsSTRING.Count()];
                for (int i = 0; i < checkedPlayersNumsSTRING.Count(); i++)
                    checkedPlayersNums[i] = Convert.ToInt32(checkedPlayersNumsSTRING[i]);
            }
            arrayListsOfPlayersRounds = new List<TextView>[checkedPlayersNums.Count()];
            arrayViewsPlayers = new View[checkedPlayersNums.Count()];
            FillLinearLayoutByPlayers(linearLayoutMatch);

            //doc = dc.ReadDocFromSDCard();
            matchXML = DBConn.Instance().Doc.Root.Elements("match").Where(r => r.Attribute("num").Value == match.Num.ToString()).First();
            List<XElement> roundsXML = (from r in matchXML.Elements("round")
                                        orderby Convert.ToInt32(r.Attribute("num").Value) ascending
                                        select r).ToList();
            foreach (XElement roundXML in roundsXML)
            {
                int roundXMLGamingPlayerPosition = Convert.ToInt32(roundXML.Attribute("gamingplayerposition").Value);
                string roundXMLroundkindname = roundXML.Attribute("roundkindname").Value;
                string[] roundResultsSTRING = roundXML.Attribute("results").Value.Split('_');
                int[] roundResults = new int[roundResultsSTRING.Count()];
                for (int s = 0; s < roundResultsSTRING.Count(); s++)
                    roundResults[s] = Convert.ToInt32(roundResultsSTRING[s]);
                ApplyRoundResultsToView(roundXMLGamingPlayerPosition, roundXMLroundkindname, roundResults, false);
                match.NextGamingPlayerPosition();
            }
        }
        private void FillLinearLayoutByPlayers(LinearLayout _linearLayoutMatch)
        {
            //XDocument doc = dc.ReadDocFromSDCard();
            for (int i = 0; i < checkedPlayersNums.Count(); i++)
            {
                string name = DBConn.Instance().Doc.Root.Elements("player")
                    .Where(r => Convert.ToInt32(r.Attribute("num").Value) == checkedPlayersNums[i])
                    .Select(r => r.Attribute("name").Value.ToString()).First();
                match.AddPlayer(checkedPlayersNums[i], name);
                _linearLayoutMatch.AddView(GeneratePlayerLayout(match.Players[i], i));
            }
        }
        private View GeneratePlayerLayout(Player player, int _i)
        {
            View view = LayoutInflater.Inflate(Resource.Layout.layoutPlayerInMatch, null);
            TextView textViewName = view.FindViewById<TextView>(Resource.Id.textViewName);
            textViewName.Text = player.Name;
            List<TextView> listTV = new List<TextView>();
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewNV));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewCh));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewMa));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewDe));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewPV));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewKi));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewk1));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewk2));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewk3));
            listTV.Add(view.FindViewById<TextView>(Resource.Id.textViewk4));

            foreach (TextView currTV in listTV)
                currTV.Click +=
                    delegate
                    #region
                    {
                        if (match.GamingPlayerPosition == player.Position)
                        {
                            if (!IsSelected(currTV))
                            {
                                match.SelectedRoundKindName = currTV.Text;
                                Intent intent = new Intent(this, typeof(activityRound));
                                intent.PutExtra("gamingPlayerPosition", match.GamingPlayerPosition);
                                intent.PutExtra("roundKindName", match.SelectedRoundKindName);
                                intent.PutExtra("matchPlayersCount", match.Players.Count);
                                int k = 0;
                                string[] playersNames = new string[match.Players.Count];
                                foreach (Player p in match.Players)
                                    playersNames[k++] = p.Name;
                                intent.PutExtra("playersNames", playersNames);
                                StartActivityForResult(intent, 1);
                            }
                            else
                                Toast.MakeText(this, "Эта игра уже сыграна", ToastLength.Short).Show();
                        }
                        else
                            Toast.MakeText(this, "Сейчас играет " + match.Players[match.GamingPlayerPosition].Name, ToastLength.Short).Show();
                    };
                    #endregion
            arrayListsOfPlayersRounds[_i] = listTV;
            view.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent, 1);
            arrayViewsPlayers[_i] = view;
            return view;
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok && requestCode == 1)
            {
                ApplyRoundResultsToView(match.GamingPlayerPosition, match.SelectedRoundKindName, data.Extras.GetIntArray("roundResults"), true);
                match.NextGamingPlayerPosition();
            }
        }
        private void AddMatchToData()
        {
            //XDocument doc = dc.ReadDocFromInternalMemory(OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename)));
            //XDocument doc = dc.ReadDocFromSDCard();

            if (DBConn.Instance().Doc.Root.Elements("match").Count() > 0)
                match.Num = DBConn.Instance().Doc.Root.Elements("match").Max(r => Int32.Parse(r.Attribute("num").Value)) + 1;
            string strForXml = "";
            for (int i = 0; i < checkedPlayersNums.Count(); i++)
                strForXml += checkedPlayersNums[i].ToString() + "_";
            strForXml = strForXml.Substring(0, strForXml.Length - 1);
            XElement matchXml = new XElement("match",
                new XAttribute("num", match.Num),
                new XAttribute("date", match.Created.ToString()),
                new XAttribute("players", strForXml));
            DBConn.Instance().Doc.Root.Add(matchXml);

            //dc.WriteDocToInternalMemory(doc, OpenFileOutput(dc.DataFileName, FileCreationMode.Private));
            DBConn.Instance().SaveToDB();
        }
        private void SaveRoundResultsToData(int[] _roundResults)
        {
            //XDocument doc = dc.ReadDocFromInternalMemory(OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename)));
            //XDocument doc = dc.ReadDocFromSDCard();

            XElement matchXml = DBConn.Instance().Doc.Root.Elements("match").Where(r => Convert.ToInt32(r.Attribute("num").Value) == match.Num).First();
            int roundNum = 0;
            if (matchXml.Elements("round").Count() > 0)
                roundNum = matchXml.Elements("round").Max(r => Convert.ToInt32(r.Attribute("num").Value)) + 1;
            string strForXml = "";
            for (int i = 0; i < _roundResults.Count(); i++)
                strForXml += _roundResults[i].ToString() + "_";
            strForXml = strForXml.Substring(0, strForXml.Length - 1);
            XElement roundXml = new XElement("round",
                new XAttribute("num", roundNum),
                new XAttribute("gamingplayerposition", match.GamingPlayerPosition),
                new XAttribute("roundkindname", match.SelectedRoundKindName),
                new XAttribute("results", strForXml));
            matchXml.Add(roundXml);

            //dc.WriteDocToInternalMemory(doc, OpenFileOutput(dc.DataFileName, FileCreationMode.Private));
            DBConn.Instance().SaveToDB();
        }
        private void ApplyRoundResultsToView(int _gamingPlayerPosition, string _roundKindName, int[] _roundResults, bool _saveToData)
        {
            for (int i = 0; i < match.Players.Count; i++)
                if (i == _gamingPlayerPosition)
                {
                    foreach (TextView _tv in arrayListsOfPlayersRounds[i])
                        if (_tv.Text == _roundKindName)
                        {
                            SelectRound(_tv);
                            for (int r = 0; r < _roundResults.Count(); r++)
                            {
                                if (_roundResults[r] != 0)
                                {
                                    RelativeLayout rlPlayer = (RelativeLayout)arrayViewsPlayers[r];
                                    TextView textViewTotal = rlPlayer.FindViewById<TextView>(Resource.Id.textViewTotal);
                                    textViewTotal.Text = (Convert.ToInt32(textViewTotal.Text) + _roundResults[r]).ToString();
                                    TextView textView;
                                    if (_roundResults[r] < 0)
                                        textView = rlPlayer.FindViewById<TextView>(Resource.Id.textViewMinus);
                                    else
                                        textView = rlPlayer.FindViewById<TextView>(Resource.Id.textViewPlus);
                                    int currPlayerResultModul = Math.Abs(_roundResults[r]);
                                    if (textView.Text == "0.")
                                        textView.Text = "";
                                    if (!textView.Text.Contains('.'))
                                        textView.Text += currPlayerResultModul.ToString() + ".";
                                    else
                                        textView.Text += (Convert.ToInt32(textView.Text.Split('.')[textView.Text.Split('.').Count() - 2]) + currPlayerResultModul).ToString() + ".";
                                }
                            }
                            if (_saveToData)
                                SaveRoundResultsToData(_roundResults);
                            break;
                        }
                    break;
                }
        }
        private void SelectRound(TextView _tv)
        {
            _tv.Text = "(" + _tv.Text + ")";
        }
        private bool IsSelected(TextView _tv)
        {
            if (_tv.Text.ToString().Contains("("))
                return true;
            else
                return false;
        }
    }
}