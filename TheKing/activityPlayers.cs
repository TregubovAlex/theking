﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Views.InputMethods;

namespace TheKing
{
    [Activity]//(Label = "activityPlayers")]
    internal class activityPlayers : Activity
    {
        DBConn dc = DBConn.Instance(); //new DataController();
        ListView listViewPlayers;
        ListView listViewPlayersInToMatch;
        List<string> listPlayers;
        List<string> listPlayersInToMatch = new List<string>();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.layoutPlayers);

            listViewPlayers = FindViewById<ListView>(Resource.Id.listViewPlayers);
            listViewPlayersInToMatch = FindViewById<ListView>(Resource.Id.listViewPlayersInToMatch);
            FillPlayersListView();

            listViewPlayers.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            #region
            {
                listPlayers.RemoveAt(e.Position);
                ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayers);
                listViewPlayers.Adapter = adapter;

                listPlayersInToMatch.Add(((TextView)e.View).Text);
                ArrayAdapter<string> adapterToMatch = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayersInToMatch);
                listViewPlayersInToMatch.Adapter = adapterToMatch;
            };
            #endregion
            listViewPlayersInToMatch.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            #region
            {
                listPlayersInToMatch.RemoveAt(e.Position);
                ArrayAdapter<string> adapterToMatch = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayersInToMatch);
                listViewPlayersInToMatch.Adapter = adapterToMatch;

                listPlayers.Add(((TextView)e.View).Text);
                ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayers);
                listViewPlayers.Adapter = adapter;
            };
            #endregion
            Button buttonCreatePlayer = FindViewById<Button>(Resource.Id.buttonCreatePlayer);
            buttonCreatePlayer.Click +=
                delegate
                #region
                {
                    LinearLayout LinearLayoutPlayerAdding = (LinearLayout)LayoutInflater.Inflate(Resource.Layout.layoutPlayerAdding, null);
                    LinearLayout linearLayoutPlayers = FindViewById<LinearLayout>(Resource.Id.linearLayoutPlayers);
                    LinearLayoutPlayerAdding.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent, 0);
                    linearLayoutPlayers.AddView(LinearLayoutPlayerAdding);

                    EditText edittextNewPlayerName = LinearLayoutPlayerAdding.FindViewById<EditText>(Resource.Id.editTextPlayerName);
                    edittextNewPlayerName.RequestFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                    inputMethodManager.ShowSoftInput(edittextNewPlayerName, ShowFlags.Forced);

                    Button buttonConfirmPlayerAdding = LinearLayoutPlayerAdding.FindViewById<Button>(Resource.Id.buttonConfirmPlayerAdding);
                    buttonConfirmPlayerAdding.Click +=
                        delegate
                        #region
                        {
                            //XDocument doc = dc.ReadDocFromInternalMemory(OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename)));
                            //XDocument doc = dc.ReadDocFromSDCard();

                            int playerNum = 0;
                            if (DBConn.Instance().Doc.Root.Elements("player").Count() > 0)
                                playerNum = DBConn.Instance().Doc.Root.Elements("player").Max(r => Int32.Parse(r.Attribute("num").Value)) + 1;
                            XElement record = new XElement("player",
                                new XAttribute("num", playerNum),
                                new XAttribute("name", edittextNewPlayerName.Text));
                            DBConn.Instance().Doc.Root.Add(record);

                            //dc.WriteDocToInternalMemory(doc, OpenFileOutput(dc.DataFileName, FileCreationMode.Private));
                            DBConn.Instance().SaveToDB();

                            listPlayers.Add(playerNum.ToString() + ": " + edittextNewPlayerName.Text);
                            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayers);
                            listViewPlayers.Adapter = adapter;

                            inputMethodManager.HideSoftInputFromWindow(edittextNewPlayerName.WindowToken, 0);
                            linearLayoutPlayers.RemoveView(LinearLayoutPlayerAdding);
                        };
                        #endregion
                };
                #endregion
            Button buttonBeginMatch = FindViewById<Button>(Resource.Id.buttonBeginMatch);
            buttonBeginMatch.Click +=
                delegate
                #region
                {
                    if (listPlayersInToMatch.Count >= 3)
                    {
                        int[] checkedPlayersNums = new int[listPlayersInToMatch.Count];
                        for (int i = 0; i < listPlayersInToMatch.Count; i++)
                            checkedPlayersNums[i] = Convert.ToInt32(listPlayersInToMatch[i].Split(':').First());
                        Intent intent = new Intent(this, typeof(activityMatch));
                        intent.PutExtra("isNewMatch", true);
                        intent.PutExtra("checkedPlayersNums", checkedPlayersNums);
                        StartActivity(intent);
                        this.Finish();
                    }
                    else
                        Toast.MakeText(this, "Выберите трёх игроков минимум", ToastLength.Short).Show();
                };
                #endregion
        }
        private void FillPlayersListView()
        {
            //XDocument doc = dc.ReadDocFromInternalMemory(OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename)));
            //XDocument doc = dc.ReadDocFromSDCard();

            IEnumerable<string> nums = from r in DBConn.Instance().Doc.Root.Elements("player")
                                       orderby Convert.ToInt32(r.Attribute("num").Value) ascending
                                       select r.Attribute("num").Value.ToString();
            IEnumerable<string> names = from r in DBConn.Instance().Doc.Root.Elements("player")
                                        orderby Convert.ToInt32(r.Attribute("num").Value) ascending
                                        select r.Attribute("name").Value.ToString();
            string[] arrNums = nums.ToArray();
            string[] arrNames = names.ToArray();
            for (int i = 0; i < arrNums.Count(); i++)
                arrNames[i] = arrNums[i] + ": " + arrNames[i];
            listPlayers = arrNames.ToList();
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listPlayers);
            listViewPlayers.Adapter = adapter;
        }
    }
}