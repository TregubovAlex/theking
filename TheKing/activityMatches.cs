﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Xml.Linq;

namespace TheKing
{
    [Activity(Label = "activityMatches")]
    public class activityMatches : Activity
    {
        DBConn dc = DBConn.Instance(); //new DataController();
        ListView listView;
        List<string> listMatches = new List<string>();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.layoutMatches);

            listView = FindViewById<ListView>(Resource.Id.listViewMatches);
            listView.ChoiceMode = ChoiceMode.None;
            UpdateMatchesListView();

            listView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
                #region
                {
                    int matchNum = Convert.ToInt32(((TextView)e.View).Text.Split(':').First());
                    Intent intent = new Intent(this, typeof(activityMatch));
                    intent.PutExtra("isNewMatch", false);
                    intent.PutExtra("matchNum", matchNum);
                    StartActivity(intent);
                    this.Finish();
                };
                #endregion
        }
        private void UpdateMatchesListView()
        {
            //XDocument doc = dc.ReadDocFromInternalMemory(OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename)));
            //XDocument doc = dc.ReadDocFromSDCard();
            IEnumerable<string> dates = from r in DBConn.Instance().Doc.Root.Elements("match")
                                        orderby Convert.ToDateTime(r.Attribute("date").Value) descending
                                        select r.Attribute("date").Value.ToString();
            IEnumerable<string> nums = from r in DBConn.Instance().Doc.Root.Elements("match")
                                       orderby Convert.ToDateTime(r.Attribute("date").Value) descending
                                       select r.Attribute("num").Value.ToString();
            IEnumerable<string> players = from r in DBConn.Instance().Doc.Root.Elements("match")
                                          orderby Convert.ToDateTime(r.Attribute("date").Value) descending
                                          select r.Attribute("players").Value.ToString();
            string[] matches = new string[dates.Count()];
            for (int i = 0; i < dates.Count(); i++)
            {
                string[] playersNums = players.ToList()[i].Split('_');
                string playersNames = "";
                for (int p = 0; p < playersNums.Count(); p++)
                {
                    string name = (from r in DBConn.Instance().Doc.Root.Elements("player")
                                   where r.Attribute("num").Value == playersNums[p]
                                   select r.Attribute("name").Value.ToString()).First();
                    if (playersNames == "")
                        playersNames += name;
                    else
                        playersNames += ", " + name;
                }
                matches[i] = nums.ToList()[i] + ": " + Convert.ToDateTime(dates.ToList()[i]).ToString("dd.MM.yyyy") + "   " + playersNames;
            }
            listMatches = matches.ToList();
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listMatches);
            listView.Adapter = adapter;
        }
    }
}