﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace TheKing
{
    [Activity]//(Label = "activityRound")
    internal class activityRound : Activity
    {
        Round round;
        List<TextView> listTVCardsInRound = new List<TextView>();
        string[] matchPlayersNames;
        List<TextView>[] arrayListsOfPlayersCards;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.layoutRound);
            LinearLayout linearLayoutRound = FindViewById<LinearLayout>(Resource.Id.linearLayoutRound);

            matchPlayersNames = Intent.Extras.GetStringArray("playersNames");
            int gamingPlayerPosition = Intent.Extras.GetInt("gamingPlayerPosition");
            round = new Round(Intent.Extras.GetString("roundKindName"));
            arrayListsOfPlayersCards = new List<TextView>[matchPlayersNames.Count()];
            round.Results = new int[matchPlayersNames.Count()];

            TextView textViewRoundFullName = linearLayoutRound.FindViewById<TextView>(Resource.Id.textViewRoundFullName);
            textViewRoundFullName.Text = round.Kind.FullName;

            SetupCardsInRoundLayout();

            for (int i = 0; i < matchPlayersNames.Count(); i++)
                if ((i >= gamingPlayerPosition && i <= gamingPlayerPosition + 2) || //играющий и игроки ниже играющего
                    (i - gamingPlayerPosition + matchPlayersNames.Count() < 3)) //игроки выше играющего
                {
                    LinearLayout playerLinearLayout = (LinearLayout)LayoutInflater.Inflate(Resource.Layout.layoutPlayerInRound, null);
                    SetupPlayerLayout(playerLinearLayout, i);
                    linearLayoutRound.AddView(playerLinearLayout);
                }
        }

        private void SetupCardsInRoundLayout()
        {
            LinearLayout linearLayoutCardsInRound = FindViewById<LinearLayout>(Resource.Id.linearLayoutCardsInRound);
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard1));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard2));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard3));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard4));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard5));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard6));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard7));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard8));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard9));
            listTVCardsInRound.Add(linearLayoutCardsInRound.FindViewById<TextView>(Resource.Id.textViewCard10));
            List<TextView> tempCardsInRound = new List<TextView>(listTVCardsInRound);
            foreach (TextView currTV in tempCardsInRound)
            {
                if (GetNumberOfCard(currTV) > round.Kind.NumOfCards)
                {
                    linearLayoutCardsInRound.RemoveView(currTV);
                    listTVCardsInRound.Remove(currTV);
                }
                SelectCard(currTV);
            }
            Button buttonSave = FindViewById<LinearLayout>(Resource.Id.linearLayoutHead).FindViewById<Button>(Resource.Id.buttonSaveRound);
            buttonSave.Click +=
                delegate
                #region
                {
                    if (GetSelectedCards(listTVCardsInRound) == 0)
                    {
                        for (int i = 0; i < matchPlayersNames.Count(); i++)
                            round.Results[i] = GetSelectedCards(arrayListsOfPlayersCards[i]) * round.Kind.PriceForCard;
                        Intent intent = new Intent();
                        intent.PutExtra("roundResults", round.Results);
                        SetResult(Result.Ok, intent);
                        this.Finish();
                    }
                    else
                        Toast.MakeText(this, "Распределите все взятки игры", ToastLength.Short).Show();
                };
                #endregion
        }
        private void SetupPlayerLayout(LinearLayout _playerLinearLayout, int _i)
        {
            TextView playerNameTV = _playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewName);
            playerNameTV.Text = matchPlayersNames[_i];

            List<TextView> listTV = new List<TextView>();
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard1));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard2));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard3));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard4));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard5));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard6));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard7));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard8));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard9));
            listTV.Add(_playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard10));

            List<TextView> tempListTV = new List<TextView>(listTV);
            foreach (TextView currTV in tempListTV)
            {
                currTV.Click +=
                    delegate
                    #region
                    {
                        DeselectAllCards(listTV);
                        SelectAllPossibleCards(listTV, currTV);
                        UpdateCardsInRound();
                    };
                    #endregion
                if (GetNumberOfCard(currTV) > round.Kind.NumOfCards)
                {
                    _playerLinearLayout.RemoveView(currTV);
                    listTV.Remove(currTV);
                }
            }
            _playerLinearLayout.FindViewById<TextView>(Resource.Id.textViewCard0).Click +=
                delegate
                #region
                {
                    DeselectAllCards(listTV);
                    UpdateCardsInRound();
                };
                #endregion
            arrayListsOfPlayersCards[_i] = listTV;
            _playerLinearLayout.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent, 1);
        }
        private void SelectCard(TextView _tv)
        {
            //_tv.Background = Drawable;
            
            _tv.Text += "=";
        }
        private void DeselectAllCards(List<TextView> _listTV)
        {
            foreach (TextView _currTV in _listTV)
                if (IsSelectedCard(_currTV))
                    _currTV.Text = _currTV.Text.ToString().Substring(0, _currTV.Text.ToString().Length - 1);
        }
        private bool IsSelectedCard(TextView _tv)
        {
            if (_tv.Text.ToString().Contains("="))
                return true;
            else
                return false;
        }
        private int GetNumberOfCard(TextView _tv)
        {
            int _number;
            if (IsSelectedCard(_tv))
                _number = Convert.ToInt32(_tv.Text.ToString().Substring(0, _tv.Text.ToString().Length - 1));
            else
                _number = Convert.ToInt32(_tv.Text.ToString());
            return _number;
        }
        private void SetSelectedCards(List<TextView> _listTV, int _numOfSelectedCards)
        {
            foreach (TextView _currTV in _listTV)
                if (GetNumberOfCard(_currTV) <= _numOfSelectedCards)
                    SelectCard(_currTV);
        }
        private int GetSelectedCards(List<TextView> _listTV)
        {
            if (_listTV == null) return 0;
            int _num = 0;
            foreach (TextView _currTV in _listTV)
                if (IsSelectedCard(_currTV))
                    _num++;
            return _num;
        }
        private int GetSumAllPlayersSelectedCards()
        {
            int _num = 0;
            for (int i = 0; i < matchPlayersNames.Count(); i++)
                _num += GetSelectedCards(arrayListsOfPlayersCards[i]);
            return _num;
        }
        private void SelectAllPossibleCards(List<TextView> _listTV, TextView _tv)
        {
            if (GetNumberOfCard(_tv) + GetSumAllPlayersSelectedCards() <= round.Kind.NumOfCards)
                SetSelectedCards(_listTV, GetNumberOfCard(_tv));
            else
                SetSelectedCards(_listTV, round.Kind.NumOfCards - GetSumAllPlayersSelectedCards());
        }
        private void UpdateCardsInRound()
        {
            DeselectAllCards(listTVCardsInRound);
            SetSelectedCards(listTVCardsInRound, round.Kind.NumOfCards - GetSumAllPlayersSelectedCards());
        }
    }
}