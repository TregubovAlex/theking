﻿using System;
using System.IO;
using System.Xml.Linq;
using Android.Content;
using Android.Content.Res;
using Android.Widget;

namespace TheKing
{
    internal sealed class DBConn //Singleton
    {
        private static DBConn _instance;
        private DBConn()
        {
            ReadDocFromSDCard();
        }
        internal static DBConn Instance()
        {
            if (_instance == null)
                lock (typeof(DBConn))
                    if (_instance == null)
                        _instance = new DBConn();
            return _instance;
        }

        private string dataFileName = "TheKing_data.xml";
        internal XDocument Doc;
        private void ReadDocFromSDCard()
        {
            string sdState = Android.OS.Environment.ExternalStorageState;
            if (sdState.Equals(Android.OS.Environment.MediaMounted))
            {
                string folder = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;

                DirectoryInfo di = new DirectoryInfo(folder + "/TheKing/");
                if (!di.Exists)
                    di.CreateSubdirectory(di.FullName);
                
                FileInfo fi = new FileInfo(folder + "/TheKing/" + dataFileName);
                if (!fi.Exists)
                    CreateBlankDataFile();

                Doc = XDocument.Load(folder + "/TheKing/" + dataFileName);
            }
        }
        internal void SaveToDB()
        {
            WriteDocToSDCard();
        }
        internal void WriteDocToSDCard()
        {
            string folder = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            Doc.Save(folder + "/TheKing/" + dataFileName);
        }
        internal void CreateBlankDataFile()
        {
            //XDocument _doc = new XDocument(new XElement("database"));
            int XMLRecord = 0;
            XDocument _doc = new XDocument(new XElement("database",
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "ноль")),
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "один")),
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "два")),
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "три")),
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "четыре")),
                new XElement("player",
                    new XAttribute("num", XMLRecord++),
                    new XAttribute("name", "пять"))
            ));
            Doc = _doc;
            SaveToDB();
            /*try
            {
                Stream tempCheckFileStream = OpenFileInput(dc.DataFileName);
                tempCheckFileStream.Close();
                Toast.MakeText(this, "Создан пустой файл базы данных", ToastLength.Short).Show();
            }
            catch (Java.IO.FileNotFoundException ex)
            {
                Toast.MakeText(this, "Ошибка создания пустой базы: " + ex.Message, ToastLength.Short).Show();
            }*/
        }
        /*internal XDocument ReadDocFromInternalMemory(Stream _streamOpenFileInput)
        {
            Stream readOldFileStream = _streamOpenFileInput;//OpenFileInput(Resources.GetString(Resource.String.DatabaseFilename));
            XDocument doc = XDocument.Load(readOldFileStream);
            readOldFileStream.Close();
            return doc;
        }*/
        /*internal void WriteDocToInternalMemory(XDocument _doc, Stream _streamOpenFileOutput)
        {
            Stream inputStream = new MemoryStream();
            _doc.Save(inputStream);
            inputStream.Position = 0;
            Stream outputStream = _streamOpenFileOutput;// OpenFileOutput(Resources.GetString(Resource.String.DatabaseFilename), FileCreationMode.Private);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.Read(buf, 0, buf.Length)) > 0)
                outputStream.Write(buf, 0, len);
            inputStream.Close();
            outputStream.Close();
        }*/
        /*internal void BackUpDatabaseToSDCard(Stream _streamOpenFileInput)
        {
            string sdState = Android.OS.Environment.ExternalStorageState;
            if (sdState.Equals(Android.OS.Environment.MediaMounted))
            {
                //XDocument doc = ReadDocFromInternalMemory(_streamOpenFileInput);
                XDocument doc = ReadDocFromSDCard();

                string folder = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
                doc.Save(folder + "/TheKing_data_" +
                    DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xml");
            }
        }*/
    }
}
